package tw.com.tsl.workshop.gettingstarted.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import tw.com.tsl.workshop.gettingstarted.model.UserEntity;

@Repository
public interface UserRepository
    extends PagingAndSortingRepository<UserEntity, Long>,
    UserMyBatisRepository {

}
