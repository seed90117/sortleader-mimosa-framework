package tw.com.tsl.workshop.gettingstarted.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import tw.com.prulife.mimosa.http.APIResponse;
import tw.com.tsl.workshop.gettingstarted.model.UserDto;
import tw.com.tsl.workshop.gettingstarted.model.UserInfo;

import java.util.Collection;

@FeignClient(name = "user", url = "${url.user.service}/user")
public interface UserClient {

  @GetMapping(value = "/getAll")
  APIResponse<Collection<UserDto>> getAllUser();

  @GetMapping(value = "/get/{userId}")
  APIResponse<UserInfo> getUser(@PathVariable Long userId);

  @GetMapping(value = "/get")
  APIResponse<Collection<UserInfo>> getUser(
      @RequestParam(required = false) String firstName,
      @RequestParam(required = false) String lastName);

  @PostMapping(value = "/insert")
  APIResponse insertUser(@RequestBody UserInfo userModel);

  @PutMapping(value = "/update")
  APIResponse updateUser(@RequestBody UserInfo userModel);

  @DeleteMapping(value = "/delete/{userId}")
  APIResponse deleteUser(@PathVariable Long userId);
}
