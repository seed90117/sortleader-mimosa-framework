package tw.com.tsl.workshop.gettingstarted.term;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import tw.com.prulife.mimosa.domain.t9n.Term;

@Getter
@RequiredArgsConstructor
public enum UserTerm implements Term {
  ID_IS_NOT_EXIST("MD", "MD-S-098-00"),
  ID_IS_NOT_EMPTY("MD", "MD-S-097-00"),
  ID_CAN_NOT_GREATER_TERM_TEM("MD", "MD-S-099-00"),
  UPDATE_USER_DATA_ERROR("MD", "MD-S-001-00");

  final String category;
  final String code;
}
