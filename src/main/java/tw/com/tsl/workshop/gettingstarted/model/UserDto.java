package tw.com.tsl.workshop.gettingstarted.model;

import lombok.Data;

@Data
public class UserDto {
  private String firstName;
  private String lastName;
}
