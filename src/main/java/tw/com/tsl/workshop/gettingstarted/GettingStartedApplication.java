package tw.com.tsl.workshop.gettingstarted;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.data.jdbc.repository.config.EnableJdbcRepositories;
import tw.com.prulife.mimosa.Mimosa;
import tw.com.prulife.mimosa.MimosaBootstrap;

@EnableFeignClients
@EnableJdbcRepositories
@MimosaBootstrap
public class GettingStartedApplication {

  public static void main(String[] args) {
    Mimosa.bootstrap(GettingStartedApplication.class, args);
  }
}
