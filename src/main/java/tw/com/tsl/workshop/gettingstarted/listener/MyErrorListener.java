package tw.com.tsl.workshop.gettingstarted.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import tw.com.prulife.mimosa.web.ErrorCaughtEvent;

@Slf4j
@Component
public class MyErrorListener {

  @Async
  @EventListener
  public void onErrorCaught(ErrorCaughtEvent event) {
    log.warn("Received caught time: {}, error code: {}, error message: {},  throw: {}",
        event.getCaughtTime(),
        event.getError().getCode(),
        event.getError().getMessage(),
        event.getThrowing());
  }
}
