package tw.com.tsl.workshop.gettingstarted.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import tw.com.prulife.mimosa.http.APIResponse;
import tw.com.prulife.mimosa.tracing.Traced;
import tw.com.tsl.workshop.gettingstarted.UserMapper;
import tw.com.tsl.workshop.gettingstarted.model.UserDto;
import tw.com.tsl.workshop.gettingstarted.model.UserInfo;
import tw.com.tsl.workshop.gettingstarted.service.UserService;

import java.util.Collection;
import java.util.stream.Collectors;

@Slf4j
@Tag(name = "User")
@Traced
@RestController
@RequestMapping(value = "user")
@RequiredArgsConstructor
public class UserController {

  final UserService userService;
  final UserMapper userMapper;

  @GetMapping(value = "/getAll")
  public APIResponse<Collection<UserDto>> getAllUser() {
    return APIResponse.success(userService.getAllUser().stream().map(userMapper::toDto).collect(Collectors.toList()));
  }

  @GetMapping(value = "/get/{userId}")
  public APIResponse<UserInfo> getUser(@PathVariable Long userId) {
    return APIResponse.success(userService.getUser(userId));
  }

  @GetMapping(value = "/get")
  public APIResponse<Collection<UserInfo>> getUser(
      @RequestParam(required = false) String firstName,
      @RequestParam(required = false) String lastName) {
    return APIResponse.success(userService.getUserByCondition(firstName, lastName));
  }

  @PostMapping(value = "/insert")
  public APIResponse insertUser(@RequestBody UserInfo userModel) {
    userService.insertUser(userModel);
    return APIResponse.success();
  }

  @PutMapping(value = "/update")
  public APIResponse updateUser(@RequestBody UserInfo userModel) {
    userService.updateUser(userModel);
    return APIResponse.success();
  }

  @PutMapping(value = "/replace")
  public APIResponse replaceUser(@RequestBody UserInfo userModel) {
    try {
      userService.updateUser(userModel);
      return APIResponse.success();
    } catch (Exception e) {
      log.error("id not found", e);
      return APIResponse.error(err -> err.message(e.getMessage()).code("E001"));
    }
  }

  @DeleteMapping(value = "/delete/{userId}")
  public APIResponse deleteUser(@PathVariable Long userId) {
    userService.deleteUser(userId);
    return APIResponse.success();
  }
}
