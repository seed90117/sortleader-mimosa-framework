package tw.com.tsl.workshop.gettingstarted.model;

import lombok.Data;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;
import tw.com.prulife.mimosa.ext.data.jdbc.AbstractAuditingEntity;

@Table("USER_INFO")
@Data
public class UserEntity extends AbstractAuditingEntity<String> {
  @Column("FIRST_NAME")
  String firstName;

  @Column("LAST_NAME")
  String lastName;
}
