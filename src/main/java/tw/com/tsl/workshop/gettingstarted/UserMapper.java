package tw.com.tsl.workshop.gettingstarted;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import tw.com.tsl.workshop.gettingstarted.model.UserDto;
import tw.com.tsl.workshop.gettingstarted.model.UserEntity;
import tw.com.tsl.workshop.gettingstarted.model.UserInfo;

@Mapper
public interface UserMapper {
  UserInfo fromDto(UserDto dto);

  UserDto toDto(UserInfo user);

  @Mapping(target = "id", ignore = true)
  UserInfo copy(UserInfo from, @MappingTarget UserInfo to);

  UserEntity toEntity(UserInfo user);

  UserInfo fromEntity(UserEntity entity);
}
