package tw.com.tsl.workshop.gettingstarted.repository;

import tw.com.tsl.workshop.gettingstarted.model.UserEntity;

import java.util.Collection;

public interface UserMyBatisRepository {

  Collection<UserEntity> getUserByCondition(String firstName, String lastName);
}
