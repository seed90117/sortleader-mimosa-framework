package tw.com.tsl.workshop.gettingstarted.client;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import tw.com.prulife.mimosa.http.APIResponse;
import tw.com.tsl.workshop.gettingstarted.model.UserDto;
import tw.com.tsl.workshop.gettingstarted.model.UserInfo;

import java.util.Collection;

@Tag(name = "UserClient")
@RequestMapping("/client/user")
@RestController
@RequiredArgsConstructor
public class UserClientController {

  final UserClient userClient;

  @GetMapping(value = "/getAll")
  public APIResponse<Collection<UserDto>> getAllUser() {
    return userClient.getAllUser();
  }

  @GetMapping(value = "/get/{userId}")
  public APIResponse<UserInfo> getUser(@PathVariable Long userId) {
    return userClient.getUser(userId);
  }

  @GetMapping(value = "/get")
  public APIResponse<Collection<UserInfo>> getUser(
      @RequestParam(required = false) String firstName,
      @RequestParam(required = false) String lastName) {
    return userClient.getUser(firstName, lastName);
  }

  @PostMapping(value = "/insert")
  public APIResponse insertUser(@RequestBody UserInfo userModel) {
    return userClient.insertUser(userModel);
  }

  @PutMapping(value = "/update")
  public APIResponse updateUser(@RequestBody UserInfo userModel) {
    return userClient.updateUser(userModel);
  }

  @DeleteMapping(value = "/delete/{userId}")
  public APIResponse deleteUser(@PathVariable Long userId) {
    return userClient.deleteUser(userId);
  }

}
