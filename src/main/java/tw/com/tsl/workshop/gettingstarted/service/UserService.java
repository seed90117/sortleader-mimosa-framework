package tw.com.tsl.workshop.gettingstarted.service;

import com.fasterxml.jackson.core.type.TypeReference;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import tw.com.prulife.mimosa.ext.cache.support.MimosaCacheManager;
import tw.com.prulife.mimosa.http.APIErrorT9nException;
import tw.com.prulife.mimosa.tracing.Traced;
import tw.com.tsl.workshop.gettingstarted.repository.UserRepository;
import tw.com.tsl.workshop.gettingstarted.term.UserTerm;
import tw.com.tsl.workshop.gettingstarted.UserMapper;
import tw.com.tsl.workshop.gettingstarted.model.UserInfo;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Traced
@Service
@RequiredArgsConstructor
public class UserService {
  static final String USER_SERVICE_BASE_CACHE = "user:service";
  static final String USER_GET_ALL_CACHE = USER_SERVICE_BASE_CACHE + ":get:all:user";
  static final String USER_GET_BY_ID_CACHE = USER_SERVICE_BASE_CACHE + ":get:user:by:id";

  final UserMapper userMapper;
  final UserRepository userRepository;
  final MimosaCacheManager cacheManager;

  private static AtomicLong id = new AtomicLong();
  private static List<UserInfo> userList = new ArrayList<>();

  public List<UserInfo> getAllUser() {
    // use cache
    return cacheManager.get(
        USER_GET_ALL_CACHE,
        "get-all",
        new TypeReference<>() {
        },
        key -> {
          List<UserInfo> users = StreamSupport
              .stream(userRepository.findAll().spliterator(), false)
              .map(userMapper::fromEntity)
              .collect(Collectors.toList());
          return Optional.of(users).filter(dbUser -> !dbUser.isEmpty());
        })
        .orElseGet(Collections::emptyList);
    // spring data jdbc
    //    return StreamSupport
    //        .stream(userRepository.findAll().spliterator(), false)
    //        .map(userMapper::fromEntity)
    //        .collect(Collectors.toList());
  }

  public Collection<UserInfo> getUserByCondition(String firstName, String lastName) {
    return userRepository.getUserByCondition(firstName, lastName)
        .stream()
        .map(userMapper::fromEntity)
        .collect(Collectors.toList());
  }

  public UserInfo getUser(Long userId) {
    if (userId == null) {
      throw new APIErrorT9nException(err -> err.term(UserTerm.ID_IS_NOT_EMPTY));
    }
    //    int pos = getUserPosition(userId);
    //    if (pos == -1) {
    //      throw new APIErrorT9nException(err -> err.term(UserTerm.ID_IS_NOT_EXIST));
    //    }
    //    return userList.get(pos);

    return cacheManager.get(
        USER_GET_BY_ID_CACHE,
        userId,
        UserInfo.class,
        key -> userRepository.findById(userId).map(userMapper::fromEntity))
        .orElseThrow(() -> {
          throw new APIErrorT9nException(err -> err.term(UserTerm.ID_IS_NOT_EXIST));
        });
  }

  public boolean insertUser(UserInfo user) {
    if (user == null) {
      throw new APIErrorT9nException(err -> err.term(UserTerm.ID_IS_NOT_EMPTY));
    }
    // object list
    user.setId(id.getAndIncrement());
    userList.add(user);

    // h2 database
    userRepository.save(userMapper.toEntity(user));

    cacheManager.evict(USER_GET_ALL_CACHE);
    return true;
  }

  public boolean updateUser(UserInfo user) {
    // use memory
    userList.stream()
        .filter(dbUser -> dbUser.getId() == user.getId())
        .findFirst()
        .map(dbUser -> userMapper.copy(user, dbUser))
        .orElseThrow(() -> new APIErrorT9nException(err -> err.term(UserTerm.UPDATE_USER_DATA_ERROR)));

    // use spring data jdbc
    userRepository.findById(user.getId())
        .map(dbUser -> userMapper.toEntity(user))
        .ifPresentOrElse(userRepository::save, () -> {
          throw new APIErrorT9nException(err -> err.term(UserTerm.UPDATE_USER_DATA_ERROR));
        });
    cacheManager.evict(USER_GET_ALL_CACHE);
    cacheManager.evict(USER_GET_BY_ID_CACHE, user.getId());
    //    userRepository.findById(user.getId())
    //      .map(dbUser -> userMapper.toEntity(user))
    //      .map(userRepository::save)
    //      .orElseThrow(() -> {
    //        throw new APIErrorT9nException(err -> err.term(UserTerm.UPDATE_USER_DATA_ERROR));
    //      });
    return true;
  }

  public boolean deleteUser(Long userId) {
    if (userId == null) {
      return false;
    }

    userRepository.deleteById(userId);
    cacheManager.evict(USER_GET_ALL_CACHE);
    cacheManager.evict(USER_GET_BY_ID_CACHE, userId);

    if (userId > 10 && userId <= 20) {
      throw new APIErrorT9nException(err -> err.term(UserTerm.ID_CAN_NOT_GREATER_TERM_TEM));
    }
    if (userId > 20) {
      throw new APIErrorT9nException(err -> err.term(UserTerm.ID_CAN_NOT_GREATER_TERM_TEM));
    }
    if (!userList.removeIf(u -> Objects.equals(u.getId(), userId))) {
      throw new APIErrorT9nException(err -> err.term(UserTerm.ID_IS_NOT_EXIST));
    }
    return true;
  }

  private int getUserPosition(Long id) {
    for (int i = 0; i < userList.size(); i++) {
      UserInfo tempUser = userList.get(i);
      if (tempUser.getId() == id) {
        return i;
      }
    }
    return -1;
  }
}
