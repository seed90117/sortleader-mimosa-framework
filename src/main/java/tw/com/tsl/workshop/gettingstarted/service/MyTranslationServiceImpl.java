package tw.com.tsl.workshop.gettingstarted.service;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import tw.com.prulife.mimosa.domain.t9n.Term;
import tw.com.prulife.mimosa.domain.t9n.Translated;
import tw.com.prulife.mimosa.domain.t9n.TranslationService;

import java.util.Optional;

@Service
public class MyTranslationServiceImpl implements TranslationService {
  @Override
  public Optional<Translated> translate(@NonNull Term term) {
    if (term.getCode().equals("MD-S-099-00")) {
      return Optional.of(new MyTranslated(term.getCategory(), term.getCode(), "id can not > 10"));
    }
    return Optional.empty();
  }

  @Data
  @RequiredArgsConstructor
  static class MyTranslated implements Translated {

    final String category;
    final String code;
    final String translation;
  }
}
