package tw.com.tsl.workshop.gettingstarted.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tw.com.prulife.mimosa.tracing.Traced;

@Tag(name = "Base")
@Traced
@RestController
@RequestMapping(value = "/base")
public class BaseController {

  @Operation(summary = "Testing", description = "Say hi with name")
  @ApiResponses({
      @ApiResponse(responseCode = "200", description = "Ok"),
      @ApiResponse(responseCode = "500", description = "Server Error")
  })
  @GetMapping("/hello/{name}")
  public String hello(@Parameter(description = "User name") @PathVariable String name) {
    return "Hello " + name;
  }
}
