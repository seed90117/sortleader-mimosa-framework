package tw.com.tsl.workshop.gettingstarted.repository;

import lombok.RequiredArgsConstructor;
import org.apache.ibatis.session.SqlSession;
import tw.com.tsl.workshop.gettingstarted.model.UserEntity;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@RequiredArgsConstructor
public class UserMyBatisRepositoryImpl implements UserMyBatisRepository {

  static final String NAMESPACE = UserEntity.class.getName() + "Mapper";
  final SqlSession sqlSession;

  @Override
  public Collection<UserEntity> getUserByCondition(String firstName, String lastName) {
    Map<String, Object> map = new HashMap<>();
    map.put("firstName", firstName);
    map.put("lastName", lastName);
    return sqlSession.selectList(NAMESPACE + ".findByCondition", map);
  }
}
